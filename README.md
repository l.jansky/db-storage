# DB Storage #

Package for providing database access for resource-api package. Provides basic CRUD operations for database tables defined in XML config files provided during inicialization. Selects to the database are defined by config objects in select function parameters. It is possible to set formatting of returned data, filtering by parameters, ordering, joins.