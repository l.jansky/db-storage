import chai from 'chai';
const assert = chai.assert;

describe('FindAll filters', () => {
	it('Should be possible to findAll entity by id', async function() {
		const entityToInsert = {
			title: 'test deep'
		};

		const storage = this.getStorage('testEntity');
		await storage.insertOne(entityToInsert);
		const insertedEntity = await storage.insertOne(entityToInsert);

		const filter = [
			{
				field: 'id',
				operator: 'eq',
				value: insertedEntity.id
			}
		];

		const foundEntities = await storage.find({ filter });
		assert.deepEqual(foundEntities, [insertedEntity]);
	});

	it('Should be possible to findAll entities by ids in array', async function() {
		const entityToInsert = {
			title: 'test deep'
		};

		const storage = this.getStorage('testEntity');
		const insertedEntity1 = await storage.insertOne(entityToInsert);
		await storage.insertOne(entityToInsert);
		const insertedEntity3 = await storage.insertOne(entityToInsert);

		const filter = [
			{
				field: 'id',
				operator: 'in',
				value: [insertedEntity1.id, insertedEntity3.id]
			}
		];

		const foundEntities = await storage.find({ filter });
		assert.deepEqual(foundEntities, [insertedEntity1, insertedEntity3]);
	});

	it('should find entities filtered by related value', async function() {
		const entity1ToInsert = {
			title: 'test1',
			testEntity2: {
				title: 'related1'
			}
		};

		const entity2ToInsert = {
			title: 'test2',
			testEntity2: {
				title: 'related2'
			}
		};

		const storage = this.getStorage('testEntity');
		await storage.insertOne(entity1ToInsert);
		const insertedEntity = await storage.insertOne(entity2ToInsert);

		const filter = [
			{
				operator: 'and',
				field: 'testEntity2',
				value: [
					{
						operator: 'eq',
						field: 'title',
						value: 'related2'
					}
				]
			}
		];

		const relations = [{ name: 'testEntity2' }];

		const foundEntities = await storage.find({ filter, relations });

		assert.deepEqual(foundEntities, [insertedEntity]);
	});
});
