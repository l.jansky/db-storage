import chai from 'chai';
const assert = chai.assert;
import R from 'ramda';
import { getStorageFactory, loadStorageConfigs } from '../src/storageLoader';
import mockAdapter from '../src/mocks/mockAdapter';

describe('Storage loader', () => {
	it('Should get storage by getStorage function', async function() {
		const configs = [
			{
				name: 'testStorage',
				table: 'test_entity',
				field: [
					{
						name: 'id'
					}
				]
			}
		];

		const adapter = mockAdapter.createAdapter({});

		const getStorage = await getStorageFactory(configs, [], adapter);
		const storage = getStorage('testStorage');

		assert.isObject(storage);
	});

	it('Should load storageConfigs from given paths and core-path', async function() {
		const paths = [__dirname + '/../test-models'];
		const configs = await loadStorageConfigs(paths);

		const testEntityConfig = R.find<any>(
			config => config.name === 'testEntity'
		)(configs);
		assert.isDefined(testEntityConfig);
	});
});
