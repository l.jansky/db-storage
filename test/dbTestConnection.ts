import { getStorageFactory, loadStorageConfigs } from '../src/storageLoader';
import mockAdapter from '../src/mocks/mockAdapter';
import sqlAdapter from '@prejt/db-storage-sql';
import dbTest from '@prejt/db-test';
import path from 'path';

const dbAdapter = process.env.DB_ADAPTER || 'mock';

const connectionInfo = {
	host: 'localhost',
	user: 'root',
	password: 'password',
	database: 'db'
};

export const init = async ctx => {
	if (dbAdapter === 'sql') {
		ctx.timeout(20000);
		await dbTest.init(
			path.join(__dirname, '..', 'test-db'),
			connectionInfo,
			false
		);
	}
};

export const start = async ctx => {
	const paths = [__dirname + '/../test-models'];
	const storageConfigs = await loadStorageConfigs(paths);

	switch (dbAdapter) {
		case 'sql':
			ctx.timeout(20000);
			ctx.container = await dbTest.start();
			ctx.connection = sqlAdapter.getDbConnection(connectionInfo);
			const sqlAdapterInstance = sqlAdapter.createAdapter(ctx.connection);
			ctx.getStorage = await getStorageFactory(
				storageConfigs,
				[],
				sqlAdapterInstance
			);
			break;
		default:
			const mockAdapterInstance = mockAdapter.createAdapter({});
			ctx.getStorage = await getStorageFactory(
				storageConfigs,
				[],
				mockAdapterInstance
			);
			break;
	}
};

export const stop = async ctx => {
	if (dbAdapter === 'sql') {
		await dbTest.remove(ctx.container);
	}
};
