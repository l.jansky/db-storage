import chai from 'chai';
const assert = chai.assert;
import { loadXmlConfigsFromPaths } from '../src/configsLoader';
import { ParsedStorageConfig } from '../src/storage.types';
import R from 'ramda';

describe('Configs loader', () => {
	it('Should load configs from given paths', async function() {
		const paths = [__dirname + '/../test-models'];
		const configs = await loadXmlConfigsFromPaths(paths);

		const testEntityConfig = R.find<ParsedStorageConfig>(
			config => config.model.$.name === 'testEntity'
		)(configs);

		assert.isDefined(testEntityConfig);
	});
});
