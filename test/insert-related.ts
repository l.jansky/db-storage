import chai from 'chai';
const assert = chai.assert;

describe('Insert related', () => {
	it('Should be possible to insert entity with related entity at once', async function() {
		const entityToInsert = {
			title: 'test deep',
			testEntity2: {
				title: 'test deep2'
			}
		};

		const storage = this.getStorage('testEntity');
		const insertedEntity = await storage.insertOne(entityToInsert);

		assert.isDefined(insertedEntity.id);
		assert.isDefined(insertedEntity.test_entity2_id);
		assert.equal(insertedEntity.test_entity2_id, insertedEntity.testEntity2.id);

		const foundEntity = await storage.findOneById(insertedEntity.id);
		assert.isUndefined(foundEntity.testEntity2);

		const relatedStorage = this.getStorage('testEntity2');
		const foundRelatedEntity = await relatedStorage.findOneById(
			insertedEntity.test_entity2_id
		);
		assert.isNotNull(foundRelatedEntity);
	});

	it('Should be possible to insert entity with related entities at once', async function() {
		const entityToInsert = {
			title: 'test deep',
			testEntityRelated: {
				title: 'test deep2',
				testEntity: {
					title: 'test deep3'
				}
			}
		};

		const storage = this.getStorage('testEntityRelated2');
		const insertedEntity = await storage.insertOne(entityToInsert);

		const deepEntityId = insertedEntity.testEntityRelated.testEntity.id;
		const deepEntityStorage = this.getStorage('testEntity');
		const deepEntity = await deepEntityStorage.findOneById(deepEntityId);
		assert.equal(
			deepEntity.title,
			entityToInsert.testEntityRelated.testEntity.title
		);
	});

	it('Should be possible to update entity with related entity at once', async function() {
		const entityToInsert = {
			title: 'test deep'
		};

		const relatedEntityToInsert = {
			title: 'test deep2'
		};

		const storage = this.getStorage('testEntity');
		const insertedEntity = await storage.insertOne(entityToInsert);

		const updatedEntity = await storage.updateOneById(insertedEntity.id, {
			testEntity2: relatedEntityToInsert
		});

		assert.isDefined(updatedEntity.testEntity2.id);
		assert.isDefined(updatedEntity.test_entity2_id);
		assert.equal(updatedEntity.test_entity2_id, updatedEntity.testEntity2.id);
		assert.equal(updatedEntity.id, insertedEntity.id);

		const foundEntity = await storage.findOneById(insertedEntity.id);
		assert.isUndefined(foundEntity.testEntity2);

		const relatedStorage = this.getStorage('testEntity2');
		const foundRelatedEntity = await relatedStorage.findOneById(
			updatedEntity.test_entity2_id
		);
		assert.isNotNull(foundRelatedEntity);

		const updatedEntity2 = await storage.updateOneById(insertedEntity.id, {
			testEntity2: { title: 'test deep3' }
		});

		assert.equal(updatedEntity2.testEntity2.title, 'test deep3');
		assert.equal(updatedEntity2.testEntity2.id, updatedEntity.test_entity2_id);

		const foundRelatedEntity2 = await relatedStorage.findOneById(
			updatedEntity.test_entity2_id
		);
		assert.equal(foundRelatedEntity2.title, 'test deep3');
	});

	it('Should be possible to insert more related entities at once', async function() {
		const entityToInsert = {
			title: 'test deep',
			testEntityRelated: [
				{
					title: 'deep related1',
					testEntityRelated2: [
						{
							title: 'deeper related 1'
						}
					]
				},
				{
					title: 'deep related2',
					testEntityRelated2: [
						{
							title: 'deeper related 2'
						}
					]
				}
			]
		};

		const storage = this.getStorage('testEntity');
		const insertedEntity = await storage.insertOne(entityToInsert);

		assert.isDefined(insertedEntity.id);
		assert.isArray(insertedEntity.testEntityRelated);

		const relatedStorage = this.getStorage('testEntityRelated');

		const filter = [
			{
				field: 'test_entity_id',
				operator: 'eq',
				value: insertedEntity.id
			}
		];

		const relatedEntities = await relatedStorage.find({ filter });

		assert.equal(
			relatedEntities.length,
			entityToInsert.testEntityRelated.length
		);
		const expectedRelated = insertedEntity.testEntityRelated.map(ent => ({
			id: ent.id,
			title: ent.title,
			test_entity_id: insertedEntity.id
		}));

		assert.deepEqual(relatedEntities, expectedRelated);

		const deepRelatedStorage = this.getStorage('testEntityRelated2');

		const deepFilter = [
			{
				field: 'test_entity_related_id',
				operator: 'eq',
				value: insertedEntity.testEntityRelated[0].id
			}
		];

		const deepRelatedEntities = await deepRelatedStorage.find({
			filter: deepFilter
		});

		const expectedDeepRelated = insertedEntity.testEntityRelated[0].testEntityRelated2.map(
			ent => ({
				id: ent.id,
				title: ent.title,
				test_entity_related_id: insertedEntity.testEntityRelated[0].id
			})
		);

		assert.deepEqual(deepRelatedEntities, expectedDeepRelated);
	});
});
