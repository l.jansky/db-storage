import chai from 'chai';
const assert = chai.assert;
import R from 'ramda';

import { loadXmlConfigsFromPaths } from '../src/configsLoader';
import fixturesFactory from '../src/fixtures';

const omitSqlValues = R.map(
	R.omit(['acl_id', 'alias_id', 'test_entity2_id', 'testEntityRelated'])
);
const omitSqlValuesWithId = R.map(
	R.omit(['id', 'acl_id', 'alias_id', 'test_entity2_id', 'testEntityRelated'])
);
const omitSqlValuesWithTestNumber = R.map(
	R.omit([
		'acl_id',
		'alias_id',
		'test_entity2_id',
		'testEntityRelated',
		'test_number'
	])
);

const getExpectedRelated = R.reduce<any, any>((acc, curr) => {
	return R.concat(
		acc,
		R.map<any, any>(
			R.assoc<any, any>('test_entity_id', curr.id),
			curr.testEntityRelated || []
		)
	);
}, []);

describe('Fixtures', () => {
	beforeEach(async function() {
		this.fixtures = fixturesFactory(this.getStorage);
		this.upsertTestEntityFixtures = this.fixtures.upsertStorageFixtures(
			'testEntity'
		);
		this.testEntityStorage = this.getStorage('testEntity');
	});

	describe('Single storage fixtures with id', () => {
		const testEntityFixtures = [
			{
				id: 1,
				title: 'test1',
				test_number: 1
			},
			{
				id: 2,
				title: 'test2',
				test_number: 2
			}
		];

		beforeEach(async function() {
			await this.upsertTestEntityFixtures(testEntityFixtures);
			const foundFixtures = await this.testEntityStorage.find();

			assert.deepEqual(omitSqlValues(foundFixtures), testEntityFixtures);
		});

		it('should insert new fixture, old will remain', async function() {
			const fixturesToUpsert = R.append({
				id: 3,
				title: 'test3',
				test_number: 3
			})(testEntityFixtures);

			await this.upsertTestEntityFixtures(fixturesToUpsert);
			const foundFixtures = await this.testEntityStorage.find();

			assert.deepEqual(omitSqlValues(foundFixtures), fixturesToUpsert);
		});

		it('should update fixture with same id', async function() {
			const fixturesToUpsert = R.update(1, {
				id: 2,
				title: 'test3',
				test_number: 2
			})(testEntityFixtures);

			await this.upsertTestEntityFixtures(fixturesToUpsert);
			const foundFixtures = await this.testEntityStorage.find();

			assert.deepEqual(omitSqlValues(foundFixtures), fixturesToUpsert);
		});
	});

	describe('Single storage fixtures without id', () => {
		const testEntityFixtures = [
			{
				title: 'test1',
				test_number: 1
			},
			{
				title: 'test2',
				test_number: 2
			}
		];

		beforeEach(async function() {
			await this.upsertTestEntityFixtures(testEntityFixtures);
			const foundFixtures = await this.testEntityStorage.find();

			assert.deepEqual(omitSqlValuesWithId(foundFixtures), testEntityFixtures);
		});

		it('should insert new fixture, old will remain', async function() {
			const fixturesToUpsert = R.append({
				title: 'test3',
				test_number: 2
			})(testEntityFixtures);

			await this.upsertTestEntityFixtures(fixturesToUpsert);
			const foundFixtures = await this.testEntityStorage.find();

			assert.deepEqual(omitSqlValuesWithId(foundFixtures), fixturesToUpsert);
		});
	});

	describe('Nested fixtures', () => {
		const testEntityFixtures = [
			{
				id: 1,
				title: 'test1',
				test_number: 1,
				testEntityRelated: [
					{
						title: 'related1'
					},
					{
						title: 'related2'
					}
				]
			},
			{
				id: 2,
				title: 'test2',
				test_number: 2
			}
		];

		beforeEach(async function() {
			this.testEntityRelatedStorage = this.getStorage('testEntityRelated');
			await this.upsertTestEntityFixtures(testEntityFixtures);
			const foundFixtures = await this.testEntityStorage.find();

			assert.deepEqual(
				omitSqlValues(foundFixtures),
				omitSqlValues(testEntityFixtures)
			);

			const foundRelated = await this.testEntityRelatedStorage.find();
			const expected = testEntityFixtures[0].testEntityRelated.map(
				R.assoc('test_entity_id', 1)
			);
			assert.deepEqual(R.map(R.omit(['id']))(foundRelated), expected);
		});

		it('should insert new fixture, old remain', async function() {
			const fixturesToUpsert = R.append({
				id: 3,
				title: 'test3',
				test_number: 3,
				testEntityRelated: [
					{
						title: 'related1'
					},
					{
						title: 'related2'
					}
				]
			})(testEntityFixtures);

			await this.upsertTestEntityFixtures(fixturesToUpsert);
			const foundFixtures = await this.testEntityStorage.find();

			assert.deepEqual(
				omitSqlValues(foundFixtures),
				omitSqlValues(fixturesToUpsert)
			);

			const foundRelated = await this.testEntityRelatedStorage.find();
			assert.sameDeepMembers(
				R.map(R.omit(['id']))(foundRelated),
				getExpectedRelated(fixturesToUpsert)
			);
		});

		it.skip('should insert new related fixture in existing item', async function() {
			const fixturesToUpsert = R.update(1, {
				id: 2,
				title: 'test2',
				test_number: 2,
				testEntityRelated: [
					{
						title: 'related3'
					}
				]
			})(testEntityFixtures as any);

			await this.upsertTestEntityFixtures(fixturesToUpsert);
			const foundFixtures = await this.testEntityStorage.find();

			assert.deepEqual(
				omitSqlValues(foundFixtures),
				omitSqlValues(fixturesToUpsert)
			);

			const foundRelated = await this.testEntityRelatedStorage.find();
			assert.sameDeepMembers(
				R.map(R.omit(['id']))(foundRelated),
				getExpectedRelated(fixturesToUpsert)
			);

			const fixturesToUpsert2 = R.update(1, {
				id: 2,
				title: 'test2',
				test_number: 2,
				testEntityRelated: [
					{
						title: 'related3'
					},
					{
						title: 'related4'
					}
				]
			})(fixturesToUpsert);

			await this.upsertTestEntityFixtures(fixturesToUpsert2);
			const foundFixtures2 = await this.testEntityStorage.find();

			assert.deepEqual(
				omitSqlValues(foundFixtures2),
				omitSqlValues(fixturesToUpsert2)
			);

			const foundRelated2 = await this.testEntityRelatedStorage.find();
			assert.sameDeepMembers(
				R.map(R.omit(['id']))(foundRelated2),
				getExpectedRelated(fixturesToUpsert2)
			);
		});
	});

	describe('Fixtures from files', () => {
		it('should load fixtures from files', async function() {
			const fixturesFromFiles = await loadXmlConfigsFromPaths([
				__dirname + '/../test-fixtures'
			]);
			await this.fixtures.upsertFixtures(fixturesFromFiles);
			const foundFixtures = await this.testEntityStorage.find();
			const expected = [
				{
					id: 1,
					title: 'test1'
				},
				{
					id: 2,
					title: 'test2'
				},
				{
					id: 3,
					title: 'test3'
				}
			];

			assert.deepEqual(omitSqlValuesWithTestNumber(foundFixtures), expected);

			const testEntityRelatedStorage = this.getStorage('testEntityRelated');
			const foundRelated = await testEntityRelatedStorage.find();
			const expectedRelated = [
				{
					title: 'related1',
					test_entity_id: 1
				},
				{
					title: 'related2',
					test_entity_id: 1
				},
				{
					title: 'related3',
					test_entity_id: 3
				}
			];

			assert.deepEqual(omitSqlValuesWithId(foundRelated), expectedRelated);
		});
	});
});
