import chai from 'chai';
const assert = chai.assert;
import { getStorageFactory } from '../src/storageLoader';
import mockAdapter from '../src/mocks/mockAdapter';
import validate from '../src/mocks/mockValidator';

describe('Storage validate', () => {
	const storageConfigs = [
		{
			name: 'test',
			table: 'test',
			field: [
				{
					name: 'withoutRule'
				},
				{
					name: 'withRule',
					rule: [{ type: 'required' }]
				}
			]
		}
	];

	beforeEach(async function() {
		const adapter = mockAdapter.createAdapter({});
		this.getStorage = await getStorageFactory(
			storageConfigs,
			[],
			adapter,
			validate
		);
	});

	it('Should validate data and find error', function() {
		const storage = this.getStorage('test');
		const errors = storage.validate({});

		assert.equal(errors.length, 1);
	});

	it('Should validate data without error', function() {
		const storage = this.getStorage('test');
		const errors = storage.validate({
			withRule: 'test'
		});

		assert.equal(errors.length, 0);
	});
});
