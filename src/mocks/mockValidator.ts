export default (object, rules) => {
	const errors = {};
	rules.forEach(rule => {
		if (!object[rule.field]) {
			errors[rule.field] = 'error';
		}
	});

	return errors;
};
