import R from 'ramda';
import {
	FindFilter,
	OrderConfig,
	StorageRelation,
	createStorageAdapter,
	AdapterFindParams,
	Entity,
	EntityData,
	ID
} from '../storage.types';
let storedData = {};

const findByIdFilter = (id: ID) => [
	{
		operator: 'eq',
		field: 'id',
		value: id
	}
];

const resolveNumber = value => {
	if (typeof value === 'string' && value !== '' && /^\d+$/.test(value)) {
		return parseInt(value);
	} else {
		return value;
	}
};

const operators = {
	eq: base => value => base == value,
	gt: base => value => value > base,
	gte: base => value => value >= base,
	lt: base => value => value < base,
	lte: base => value => value <= base,
	in: base => value => R.contains(value, base),
	has: base => value => {
		const ent = R.pipe(
			R.filter(resolveFilter(base)),
			R.length,
			length => length > 0
		)(value);

		return ent;
	},
	or: base => value => resolveFilter(base, false)(value),
	and: base => value => resolveFilter(base)(value),
	like: base => value => value.includes(base)
};

const resolveFilter = (filters: FindFilter[], and = true) => item => {
	return R.reduce<FindFilter, any>((resolved, filter) => {
		const operatorName = filter.operator;
		const operator = operators[operatorName](filter.value);
		const value = filter.field ? item[filter.field] : item;
		return and ? (resolved &= operator(value)) : (resolved |= operator(value));
	}, and)(filters);
};

const resolveOrder = (order: OrderConfig[]) =>
	R.sortWith(
		R.map<OrderConfig, any>(orderParam =>
			orderParam.desc
				? R.descend(R.prop(orderParam.by))
				: R.ascend(R.prop(orderParam.by))
		)(order)
	);

const setRelatedData = (relations: StorageRelation[], item) => {
	return R.reduce(
		(acc, curr) => {
			const val = findSync(curr.table)({
				filter: [
					{
						field: curr.foreignField,
						operator: 'eq',
						value: acc[curr.ownField]
					}
				],
				relations: curr.relations
			});

			return R.assoc(curr.as || curr.name, curr.hasMany ? val : val[0], acc);
		},
		item,
		relations
	);
};

const findSync = tableName => params => {
	const filter = params.filter || [];
	const limit = params.limit || Infinity;
	const offset = params.offset || 0;
	const order = params.order || [];
	const relations = params.relations || [];

	const data = storedData[tableName] || [];
	return R.pipe(
		R.map(item => setRelatedData(relations, item)),
		R.filter(resolveFilter(filter)),
		resolveOrder(order),
		R.slice(offset, offset + limit)
	)(data);
};

const find = (tableName: string) => (
	params: AdapterFindParams = {}
): Promise<Entity[]> => {
	const result = findSync(tableName)(params);
	return Promise.resolve(result);
};

const insertOne = (tableName: string) => (
	data: EntityData
): Promise<Entity> => {
	const id = data.id
		? parseInt(data.id as string)
		: storedData[tableName]
		? storedData[tableName].length
		: 0;
	const insertedData = R.pipe<
		Record<string, any>,
		Record<string, any>,
		Record<string, any>
	>(
		R.assoc('id', id),
		R.map(resolveNumber)
	)(data) as any;

	storedData = R.pipe(
		R.merge({ [tableName]: [] }),
		R.evolve<any>({ [tableName]: R.append(insertedData) })
	)(storedData);

	return find(tableName)({ filter: findByIdFilter(insertedData.id) }).then(
		foundData => foundData[0] || null
	);
};

const updateOneById = (tableName: string) => async (
	id: ID,
	data: EntityData
): Promise<Entity> => {
	const index = R.findIndex(R.propEq('id', parseInt(id as any)))(
		storedData[tableName] || []
	);
	if (index >= 0) {
		const updatedItem = R.pipe(
			R.merge(storedData[tableName][index]),
			R.map(resolveNumber)
		)(data);

		storedData = R.evolve(
			{
				[tableName]: R.update(index, updatedItem)
			},
			storedData
		);
		return (updatedItem as unknown) as Entity;
	} else {
		return null;
	}
};

const deleteOneById = (tableName: string) => (id: ID) => {
	const intId: number = parseInt(id as any);
	const index = R.findIndex(R.propEq('id', intId))(storedData[tableName] || []);

	const found = storedData[tableName] && storedData[tableName][intId];
	if (index >= 0) {
		storedData = R.evolve(
			{
				[tableName]: R.filter<any>(item => item.id !== intId)
			},
			storedData
		);

		return found;
	} else {
		return null;
	}
};

const upsertFixture = tableName => fixtureData => {
	return fixtureData;
};

const createAdapter: createStorageAdapter = db => {
	storedData = db;

	return (tableName: string) => {
		return {
			find: find(tableName),
			insertOne: insertOne(tableName),
			updateOneById: updateOneById(tableName),
			deleteOneById: deleteOneById(tableName),
			upsertFixture: upsertFixture(tableName)
		};
	};
};

export default { createAdapter };
