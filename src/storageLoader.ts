import R from 'ramda';
import { loadXmlConfigsFromPaths } from './configsLoader';
import { createStorage } from './storage';
import fixturesFactory from './fixtures';
import {
	DbStorage,
	StorageRawConfig,
	KeyValue,
	ParsedStorageConfig,
	createStorageTableAdapter,
	getStorage
} from './storage.types';

export const getStorageFactory = async (
	configs: StorageRawConfig[],
	fixtures: any,
	createStorageTableAdapter: createStorageTableAdapter,
	validate?: any
): Promise<getStorage> => {
	const getStorage: getStorage = (storageName: string) => {
		return storages[storageName] || null;
	};

	// !!! switched functions in pipe - due to types on map function
	const storages = R.pipe<StorageRawConfig[], DbStorage[], KeyValue<DbStorage>>(
		R.map(config =>
			createStorage(config, createStorageTableAdapter, getStorage, validate)
		),
		R.reduce<DbStorage, KeyValue<DbStorage>>(
			(acc, curr) =>
				Object.assign({}, acc, { [curr.storageConfig.name]: curr }),
			{}
		)
	)(configs);

	const fixturesService = fixturesFactory(getStorage);
	await fixturesService.upsertFixtures(fixtures);

	return getStorage;
};

const formatConfig = (configFromXml: ParsedStorageConfig): StorageRawConfig => {
	return R.merge(configFromXml.model.$, {
		field: configFromXml.model.field
			? configFromXml.model.field.map(f => f.$)
			: []
	});
};

export const loadStorageConfigs = R.pipe<string[], Promise<StorageRawConfig[]>>(
	paths => loadXmlConfigsFromPaths(paths).then(R.map(formatConfig))
);
