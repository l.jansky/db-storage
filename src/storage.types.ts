export type ID = number;

interface ParsedStorageConfigField {
	$: {
		name: string;
		foreign?: string;
		as?: string;
	};
}

export interface ParsedStorageConfig {
	model: {
		$: {
			name: string;
			table: string;
			acl?: string;
		};
		field: ParsedStorageConfigField[];
	};
}

export interface DbStorage {
	find: (params: FindParams) => Promise<Entity[]>;
	findOneById: (id: ID) => Promise<Entity>;
	insertOne: (data: EntityData) => Promise<Entity>;
	updateOneById: (id: ID, data: EntityData) => Promise<Entity>;
	deleteOneById: (id: ID) => Promise<Entity>;
	validate: any;
	storageConfig: StorageConfig;
}

export interface StorageConfig {
	name: string;
	table: string;
	acl?: string;
	relatedStorages: KeyValue<StorageConfigField>;
}

export interface StorageConfigField {
	name: string;
	foreign?: string;
	as?: string;
}

export interface StorageRawConfig {
	name: string;
	table: string;
	acl?: string;
	field: StorageConfigField[];
}

export interface KeyValue<T> {
	[key: string]: T;
}

export interface FindParams {
	relations?: Relation[];
	filter?: FindFilter[];
	limit?: number;
	offset?: number;
	order?: any;
}

export interface AdapterFindParams {
	relations?: StorageRelation[];
	filter?: FindFilter[];
	limit?: number;
	offset?: number;
	order?: any;
}

export interface Relation {
	name: string;
	relations?: Relation[];
}

export interface StorageRelation {
	name: string;
	table: string;
	as?: string;
	ownField: string;
	foreignField: string;
	hasMany: boolean;
	relations: StorageRelation[];
}

export type getStorage = (name: string) => DbStorage;

export interface FindFilter {
	field?: string;
	operator: string;
	value: any;
}

export interface OrderConfig {
	desc?: boolean;
	by: string;
}

export type createStorageTableAdapter = (table: string) => StorageTableAdapter;

export type createStorageAdapter = (db: any) => createStorageTableAdapter;

export interface StorageTableAdapter {
	find: (params: AdapterFindParams) => Promise<Entity[]>;
	insertOne: (data: EntityData) => Promise<Entity>;
	updateOneById: (id: ID, data: EntityData) => Promise<Entity>;
	deleteOneById: (id: ID) => Promise<Entity>;
	upsertFixture: any;
}

export interface Entity extends EntityData {
	id: ID;
}

export interface EntityData {
	[key: string]: string | number | EntityData | EntityData[];
}
