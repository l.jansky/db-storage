import R from 'ramda';
import { throwError } from './error';
import { getRelations, removeNonRelatedFilters } from './relations';
import {
	DbStorage,
	StorageConfig,
	StorageRawConfig,
	StorageConfigField,
	KeyValue,
	FindParams,
	createStorageTableAdapter,
	StorageTableAdapter,
	getStorage,
	Entity,
	EntityData,
	Relation,
	FindFilter,
	ID
} from './storage.types';

const find = (
	storageTableAdapter: StorageTableAdapter,
	storageConfig: StorageConfig,
	getStorage: getStorage
) => async (params: FindParams = {}) => {
	const relations = getRelations(
		params.relations || [],
		getStorage,
		storageConfig
	);
	const filter = removeNonRelatedFilters(relations)(params.filter || []);
	const limit = params.limit || 0;
	const offset = params.offset || 0;
	const order = params.order || [];
	const entities = await storageTableAdapter.find({
		filter,
		limit,
		offset,
		order,
		relations
	});

	return entities;
};

const findOneById = (
	storageTableAdapter: StorageTableAdapter,
	storageConfig: StorageConfig,
	getStorage: getStorage
) => async (id: ID, relations: Relation[] = []): Promise<Entity | null> => {
	const filter: FindFilter[] = [
		{
			field: 'id',
			value: id,
			operator: 'eq'
		}
	];

	const entities = await find(storageTableAdapter, storageConfig, getStorage)({
		filter,
		relations
	});
	return entities[0] || null;
};

const insertOne = (
	storageTableAdapter: StorageTableAdapter,
	storageConfig: StorageConfig,
	getStorage: getStorage
) => async (data: EntityData): Promise<Entity> => {
	const relatedStorages = storageConfig.relatedStorages;
	const dataToMergeBefore: EntityData = {};
	const dataToMergeAfter: EntityData = {};

	for (let key in data) {
		if (relatedStorages[key]) {
			const relatedStorage = getStorage(relatedStorages[key].foreign);
			// TODO: condition for object here
			const insertedSubEntity = await relatedStorage.insertOne(data[
				key
			] as Entity);
			dataToMergeBefore[relatedStorages[key].name] = insertedSubEntity.id;
			dataToMergeAfter[key] = insertedSubEntity;
		} else {
			const relatedStorage = getStorage(key);
			if (
				relatedStorage &&
				relatedStorage.storageConfig.relatedStorages[storageConfig.name]
			) {
				dataToMergeAfter[key] = [];
			}
		}
	}

	const mergedData = R.pipe<EntityData, EntityData, EntityData>(
		R.omit(R.keys(dataToMergeAfter) as string[]),
		R.merge(R.__, dataToMergeBefore)
	)(data);

	const insertedEntity = await storageTableAdapter.insertOne(mergedData);

	for (let key in data) {
		const relatedStorage = getStorage(key);
		if (
			relatedStorage &&
			relatedStorage.storageConfig.relatedStorages[storageConfig.name]
		) {
			const relatedDataList = data[key] as Entity[];
			const relatedFieldName =
				relatedStorage.storageConfig.relatedStorages[storageConfig.name].name;
			const insertedSubEntities = [];
			for (let relatedData of relatedDataList) {
				const insertedSubEntity = await relatedStorage.insertOne(
					R.assoc(relatedFieldName, insertedEntity.id, relatedData)
				);
				insertedSubEntities.push(insertedSubEntity);
			}

			dataToMergeAfter[key] = insertedSubEntities;
		}
	}

	return {
		...insertedEntity,
		...dataToMergeAfter
	};
};

const updateOneById = (
	storageTableAdapter: StorageTableAdapter,
	storageConfig: StorageConfig,
	getStorage: getStorage
) => async (id: ID, data: EntityData): Promise<Entity> => {
	const relatedStorages = storageConfig.relatedStorages;
	const entity = await findOneById(
		storageTableAdapter,
		storageConfig,
		getStorage
	)(id);

	if (!entity) {
		return null;
	}

	const dataToMergeBefore = {};
	const dataToMergeAfter = {};

	for (let key in data) {
		if (relatedStorages[key]) {
			const relatedStorage = getStorage(relatedStorages[key].foreign);
			let subEntity = null;
			if (
				typeof entity[relatedStorages[key].name] !== 'undefined' &&
				entity[relatedStorages[key].name] !== null
			) {
				// TODO: conditions for string and object
				subEntity = await relatedStorage.updateOneById(
					entity[relatedStorages[key].name] as ID,
					data[key] as Entity
				);
			} else {
				// TODO: condition for object
				subEntity = await relatedStorage.insertOne(data[key] as Entity);
			}

			if (subEntity) {
				dataToMergeBefore[relatedStorages[key].name] = subEntity.id;
				dataToMergeAfter[key] = subEntity;
			}
		} else {
			const relatedStorage = getStorage(key);
			if (
				relatedStorage &&
				relatedStorage.storageConfig.relatedStorages[storageConfig.name]
			) {
				dataToMergeAfter[key] = [];
			}
		}
	}

	const mergedData = R.pipe(
		R.omit(R.keys(dataToMergeAfter)),
		R.merge(R.__, dataToMergeBefore)
	)(data);

	const updatedEntity = await storageTableAdapter.updateOneById(id, mergedData);
	return {
		...updatedEntity,
		...dataToMergeAfter
	};
};

const deleteOneById = (storageTableAdapter: StorageTableAdapter) => (
	id: ID
): Promise<Entity> => {
	return storageTableAdapter.deleteOneById(id);
};

const validate = (fields, validateObject) => entity => {
	const rules = [];

	fields.forEach(field => {
		if (field.rule) {
			field.rule.forEach(rule => {
				rules.push({
					field: field.name,
					type: rule.type
				});
			});
		}
	});

	const errors = validateObject(entity, rules);

	return R.pipe(R.toPairs)(errors);
};

export const createStorage = (
	config: StorageRawConfig,
	createStorageTableAdapter: createStorageTableAdapter,
	getStorage: getStorage,
	validateObject
): DbStorage => {
	const storageTable = config.table;
	if (!storageTable) {
		throwError(500, 'Model without table.');
	}

	const relatedStorages = R.pipe<
		StorageConfigField[],
		StorageConfigField[],
		KeyValue<StorageConfigField>
	>(
		R.filter<StorageConfigField>(field => !!field.foreign),
		R.reduce(
			(acc, curr) => R.merge(acc, { [curr.as || curr.foreign]: curr }),
			{}
		)
	)(config.field);

	const storageTableAdapter: StorageTableAdapter = createStorageTableAdapter(
		storageTable
	);

	const storageConfig: StorageConfig = {
		name: config.name,
		table: config.table,
		acl: config.acl,
		relatedStorages
	};

	return {
		find: find(storageTableAdapter, storageConfig, getStorage),
		findOneById: findOneById(storageTableAdapter, storageConfig, getStorage),
		insertOne: insertOne(storageTableAdapter, storageConfig, getStorage),
		updateOneById: updateOneById(
			storageTableAdapter,
			storageConfig,
			getStorage
		),
		deleteOneById: deleteOneById(storageTableAdapter),
		validate: validate(config.field, validateObject),
		storageConfig
	};
};
