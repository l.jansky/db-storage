import { readdirSync, readFileSync } from 'fs';
import R from 'ramda';
import { parseString } from 'xml2js';
import { ParsedStorageConfig } from './storage.types';

const getFilesFromPath = (path: string): string[] => {
	return readdirSync(path).map(fileName =>
		readFileSync(path + '/' + fileName, 'utf8')
	);
};

const getConfigPromise = (content: string): Promise<ParsedStorageConfig> => {
	return new Promise<ParsedStorageConfig>((resolve, reject) => {
		parseString(content, (err, res) => {
			if (!err) {
				resolve(res);
			} else {
				reject(err);
			}
		});
	});
};

export const loadXmlConfigsFromPaths: (
	paths: string[]
) => Promise<ParsedStorageConfig[]> = R.pipe<
	string[],
	string[][],
	string[],
	Promise<ParsedStorageConfig>[],
	Promise<ParsedStorageConfig[]>
>(
	paths => R.map(getFilesFromPath, paths),
	filesByPath => R.reduce<string[], string[]>(R.concat, [], filesByPath),
	allFiles => R.map(getConfigPromise, allFiles),
	promisesToAllConfigs => Promise.all(promisesToAllConfigs)
);
