import R from 'ramda';
import {
	Relation,
	StorageRelation,
	getStorage,
	StorageConfig,
	FindFilter
} from './storage.types';

export const getRelations = (
	relations: Relation[],
	getStorage: getStorage,
	storageConfig: StorageConfig
): StorageRelation[] => {
	const relatedStorages = storageConfig.relatedStorages;

	return R.pipe<Relation[], Relation[], StorageRelation[]>(
		R.filter<Relation>(relation => {
			const hasMany = !relatedStorages[relation.name];
			if (hasMany) {
				const relatedStorage = getStorage(relation.name);
				return !!relatedStorage.storageConfig.relatedStorages[
					storageConfig.name
				];
			} else {
				return true;
			}
		}),
		R.map<Relation, StorageRelation>(relation => {
			const hasMany = !relatedStorages[relation.name];
			const { storageConfig: relatedStorageConfig } = hasMany
				? getStorage(relation.name)
				: getStorage(relatedStorages[relation.name].foreign);

			const as = !hasMany ? relatedStorages[relation.name].as : undefined;

			return {
				name: relatedStorageConfig.name,
				table: relatedStorageConfig.table,
				as,
				ownField: hasMany ? 'id' : relatedStorages[relation.name].name,
				foreignField: hasMany
					? relatedStorageConfig.relatedStorages[storageConfig.name].name
					: 'id',
				hasMany,
				relations: relation.relations
					? getRelations(relation.relations, getStorage, relatedStorageConfig)
					: []
			};
		})
	)(relations);
};

const isWithSubfilter = (filter: FindFilter): boolean =>
	R.contains(filter.operator, ['has', 'and', 'or']);

export const removeNonRelatedFilters = (relations: StorageRelation[]) =>
	R.pipe<FindFilter[], FindFilter[], FindFilter[], FindFilter[]>(
		R.filter<FindFilter>(f => {
			if (f.field && isWithSubfilter(f)) {
				const hasRelated = R.any(item => item.name === f.field, relations);
				return hasRelated;
			} else {
				return true;
			}
		}),
		R.map(f => {
			if (isWithSubfilter(f)) {
				let subRelations = relations;
				if (f.field) {
					const relation = R.find(item => item.name === f.field, relations);
					subRelations = relation.relations;
				}

				return R.evolve(
					{
						value: removeNonRelatedFilters(subRelations)
					},
					f
				);
			} else {
				return f;
			}
		}),
		R.filter<FindFilter>(f => !isWithSubfilter(f) || f.value.length > 0)
	);
